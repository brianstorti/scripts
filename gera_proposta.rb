#!/usr/bin/env ruby
def calc_dv(value)
  total = 0
  num_proposta = value.to_s
  len = num_proposta.size
  0.upto(len) do |i|
    char = num_proposta[i..i].to_i
    total += char * (num_proposta.size + 1 - i)
  end

  result = (total * 10) % 11
  return result == 10 ? "0" : result.to_s
end

num_proposta = (rand(9999999 - 10) + 10).to_s
num_proposta_sem_dv = num_proposta[0, num_proposta.length - 1]
puts num_proposta + "-" + calc_dv(num_proposta)
