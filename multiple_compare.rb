begin
  require 'rubygems'
  require 'spreadsheet'
  require 'optparse'
rescue LoadError
  puts "O script depende de algumas bibliotecas que não estão instaladas."
  puts "Execute 'sudo gem install spreadsheet' e tente novamente."
  exit
end

options = {}

optparse = OptionParser.new do |opts|
  options[:all] = false
  opts.on('-a', '--all', 'Exibe todos os campos, mesmo os que não possuem divergência') do
    options[:all] = true
  end
end.parse!(ARGV)

FACTORY = "factory.txt"
GABARITO = "gabarito.txt"
CVP = "cvp.txt"

class String
  ACCENTS_MAPPING = {
    'E' => [200,201,202,203],
    'e' => [232,233,234,235],
    'A' => [192,193,194,195,196,197],
    'a' => [224,225,226,227,228,229,230],
    'C' => [199],
    'c' => [231],
    'O' => [210,211,212,213,214,216],
    'o' => [242,243,244,245,246,248],
    'I' => [204,205,206,207],
    'i' => [236,237,238,239],
    'U' => [217,218,219,220],
    'u' => [249,250,251,252],
    'N' => [209],
    'n' => [241],
    'Y' => [221],
    'y' => [253,255],
    'AE' => [306],
    'ae' => [346],
    'OE' => [188],
    'oe' => [189]
  }

  def remove_accents
    str = String.new(self)
    String::ACCENTS_MAPPING.each do |letter,accents|
      packed = accents.pack('U*')
      rxp = Regexp.new("[#{packed}]", nil, 'U')
      str.gsub!(rxp, letter)
    end
    str
  end
end
@print_obs = false
factory = File.new(FACTORY)
fields = []
factory.each do |line|
  line = line.split(",")
  desc = line[2]
  desc ||= " "

  @print_obs = true unless line[2].nil?

  fields << {:field => line[0], :size => line[1], :desc => desc.strip!}
end

gabarito = File.new(GABARITO)
cvp = File.new(CVP)

lines_gabarito = []
lines_cvp = []

gabarito.each { |line|  lines_gabarito << line.remove_accents }
cvp.each { |line|  lines_cvp << line.remove_accents }

result = ""

Spreadsheet.client_encoding = 'UTF-8'
book = Spreadsheet::Workbook.new
sheet = book.create_worksheet
bold = Spreadsheet::Format.new :weight => :bold

sheet_line = 0

format = Spreadsheet::Format.new :weight => :bold
count = 0

0.upto(lines_gabarito.size - 1) do |i|
  sheet[sheet_line, 0] = "NOME CAMPO"
  sheet[sheet_line, 1] = "CVP"
  sheet[sheet_line, 2] = "GABARITO"
  sheet[sheet_line, 3] = "COL. INÍCIO"
  sheet[sheet_line, 4] = "COL. FIM"
  sheet[sheet_line, 5] = "OBS" if @print_obs

  sheet.column(0).width = 20
  sheet.column(1).width = 45
  sheet.column(2).width = 45
  sheet.column(3).width = 10
  sheet.column(4).width = 10
  sheet.column(5).width = 60

  sheet.row(sheet_line).default_format = format

  sheet_line += 1

  column = 0
  fields.each do |field|
    column_to = column + field[:size].to_i

    field_gabarito = lines_gabarito[i][column..column_to - 1]
    field_cvp =  lines_cvp[i][column..column_to - 1]
    from = column
    column = column_to

    unless options[:all]
      should_hide = (field[:desc] == "*")
      should_never_hide = (field[:desc] == "-")
      should_never_hide = false unless should_never_hide
      next if (field_gabarito == field_cvp or should_hide) and !should_never_hide
    end

    count += 1
    puts field_gabarito
    puts field_cvp
    puts "-------"

    sheet[sheet_line, 0] = field[:field]
    sheet[sheet_line, 1] = field_cvp
    sheet[sheet_line, 2] = field_gabarito
    sheet[sheet_line, 3] = from + 1
    sheet[sheet_line, 4] = column + 1
    sheet[sheet_line, 5] = field[:desc] if @print_obs

    result += "\n#{field[:field]} - Colunas #{from + 1} a #{column + 1}"
    result += "\nGabarito: #{field_gabarito}"
    result += "\nCVP:      #{field_cvp}"
    result += "\n\n"
    sheet_line += 1
  end
  sheet_line += 2
end

xls_name = ARGV[0].nil? ? "result.xls" : ARGV[0] + ".xls"

if count == 0
  puts "\nNenhuma divergência encontrada."
  puts "I believe in miracles."
else
  puts "\n#{count} divergência(s) encontrada(s)."
  book.write(xls_name)
end
