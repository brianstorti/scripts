require 'optparse'

options = {}

optparse = OptionParser.new do |opts|
  options[:c] = false
  opts.on('-c', '--compare', 'Run multiple_compare.rb') do
    options[:compare] = true
  end
end.parse!(ARGV)

criteria = File.new("criteria.txt")
cvp = File.new("cvp.txt")
gabarito = File.new("gabarito.txt")
lines_cvp = []
lines_gabarito = []
propostas = []

criteria.each {|line| propostas << line.strip!}
cvp.each {|line| lines_cvp << line.strip! }
gabarito.each {|line| lines_gabarito << line.strip! }

def create_dir(proposta)
  Dir.mkdir("compare") unless File.directory?("compare")

  proposta_dir = File.join("compare", proposta)
  Dir.mkdir(proposta_dir) unless File.directory?(proposta_dir)

  system "cp multiple_compare.rb ./compare/#{proposta}"
  system "cp factory.txt ./compare/#{proposta}"
end
@propostas_com_numero_de_linhas_divergentes = ""
propostas.each do |proposta|
  create_dir(proposta)
  new_cvp = ""
  new_gabarito = ""
  count_gabarito = 0
  count_cvp = 0
  lines_gabarito.each do |line|
    if line.include? proposta
      new_gabarito << line + "\n"
      count_gabarito += 1
    end
  end

  lines_cvp.each do |line|
    if line.include? proposta
      new_cvp << line + "\n"
      count_cvp += 1
    end
  end

  if count_cvp != count_gabarito
    @propostas_com_numero_de_linhas_divergentes << proposta + "\n"
  end

  File.open(".newcvp.txt", 'w') { |f| f.write(new_cvp) }
  File.open(".newgabarito.txt", 'w') { |f| f.write(new_gabarito) }

  system "cp .newcvp.txt ./compare/#{proposta}/cvp.txt"
  system "cp .newgabarito.txt ./compare/#{proposta}/gabarito.txt"
  system "cd ./compare/#{proposta} && ruby multiple_compare.rb result_#{proposta}" if options[:compare]
end

puts "\nPropostas com número de linhas divergentes:"
puts @propostas_com_numero_de_linhas_divergentes
