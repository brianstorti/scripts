require 'rubygems'
require "cgi"

class Meme
  def self.characters
    {"yuno" => "http://memecaptain.com/y_u_no.jpg",
     "all" => "http://memecaptain.com/all_the_things.jpg",
     "bear" => "http://memecaptain.com/bear_grylls.jpg",
     "cat" => "http://memecaptain.com/business_cat.jpg",
     "wolf" => "http://memecaptain.com/courage_wolf.jpg",
     "nerd" => "http://memecaptain.com/dwight_schrute.jpg",
     "fry" => "http://memecaptain.com/fry.png",
     "greg" => "http://memecaptain.com/good_guy_greg.jpg",
     "grandma" => "http://memecaptain.com/grandma.jpg",
     "wolf2" => "http://memecaptain.com/insanity_wolf.jpg",
     "husband" => "http://memecaptain.com/internet_husband.jpg",
     "megusta" => "http://memecaptain.com/me_gusta.png",
     "interesting" => "http://memecaptain.com/most_interesting.jpg",
     "okay" => "http://memecaptain.com/ok.png",
     "dinossaur" => "http://memecaptain.com/philosoraptor.jpg",
     "fu" => "http://memecaptain.com/rage.png",
     "scrumbag" => "http://memecaptain.com/scumbag_steve.jpg",
     "seriously" => "http://memecaptain.com/seriously.png",
     "success" => "http://memecaptain.com/success_kid.jpg",
     "troll" => "http://memecaptain.com/troll_face.jpg",
     "trolldad" => "http://memecaptain.com/trolldad.png",
     "troldaddancing" => "http://memecaptain.com/trolldad_dancing.png",
     "yao" => "http://memecaptain.com/yao_ming.jpg",
     "xzibit" => "http://memecaptain.com/xzibit.jpg",
    }
  end

  def self.generate(character, top_text, bottom_text)
    url = "http://memecaptain.com/i?u=#{CGI.escape(characters[character])}&tt=#{CGI.escape(top_text)}&tb=#{CGI.escape(bottom_text)}"
    puts url
    if RUBY_PLATFORM.downcase.include?("linux")
      #IO.popen('xsel –clipboard –input', 'r+') { |clipboard| clipboard.puts url }
    elsif RUBY_PLATFORM.downcase.include?("darwin")
      IO.popen('pbcopy', 'r+') { |clipboard| clipboard.puts url }
    end
  end
end

def help
  puts "Sintaxe: ruby meme.rb [MEME] ['FRASE TOPO'] ['FRASE BASE']\n\n"
  puts "Possíveis memes:"
  Meme.characters.each_pair do |key, value|
    puts key
  end
end

if ARGV.include?("-h") || ARGV.size != 3
  help
end

begin
  Meme.generate(*ARGV)
rescue
  help
end
